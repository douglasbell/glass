package com.dugbel.glass.addcontact;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dugbel.glass.exception.GeocodingException;
import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;
//import android.widget.TextView;

public class AddContactActivity extends Activity {


	private static final String TAG = AddContactActivity.class.getSimpleName();
	private static final int TAKE_PHOTO_REQUEST_CODE = 1;

	private static final int NAME_REQUEST_CODE = 2;
	private static final int PHONE_REQUEST_CODE = 3;
	private static final int EMAIL_REQUEST_CODE = 4;
	private static final int ADDRESS_REQUEST_CODE = 5;

	private static final String IMAGE_FILE_NAME = Environment.getExternalStorageDirectory().getPath()  + "/Pictures/contact_image.jpg";

	private boolean picTaken = false; // flag to indicate if we just returned from the picture taking intent

	private TextView text1;
	private TextView text2;

	private ProgressBar myProgressBar;
	protected boolean mbActive;

	final Handler myHandler = new Handler(); // handles looking for the returned image file

	private TextToSpeech mSpeech;

	private GestureDetector mGestureDetector;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.v(TAG,"creating activity");

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.activity_addcontact);
		text1 = (TextView) findViewById(R.id.text1);
		text2 = (TextView) findViewById(R.id.text2);
		text1.setText("");
		text2.setText("");
		myProgressBar = (ProgressBar) findViewById(R.id.my_progressBar);
		LinearLayout llResult = (LinearLayout) findViewById(R.id.resultLinearLayout);
		TextView tvResult = (TextView) findViewById(R.id.tap_instruction);
		llResult.setVisibility(View.INVISIBLE);
		tvResult.setVisibility(View.INVISIBLE);
		myProgressBar.setVisibility(View.INVISIBLE);

		// Even though the text-to-speech engine is only used in response to a menu action, we
		// initialize it when the application starts so that we avoid delays that could occur
		// if we waited until it was needed to start it up
		//        mSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
		//            @Override
		//            public void onInit(int status) {
		//                mSpeech.speak("Contact photo captured.", TextToSpeech.QUEUE_FLUSH, null);
		//            }
		//        });

		mGestureDetector = createGestureDetector(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (!picTaken) {
			Intent intent = new Intent(this, CameraActivity.class);
			intent.putExtra("imageFileName",IMAGE_FILE_NAME);
			startActivityForResult(intent,1);
		}
		else {
			// do nothing
		}
	}

	/*
	 * Send generic motion events to the gesture detector
	 */
	@Override
	public boolean onGenericMotionEvent(MotionEvent event) {
		if (mGestureDetector != null) {
			return mGestureDetector.onMotionEvent(event);
		}
		return false;
	}

	/**
	 * 
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws GeocodingException
	 */
	private List<Address> geocode(final double latitude, final double longitude) 
			throws GeocodingException {
		try {
			Geocoder geocoder = new Geocoder(this, Locale.getDefault());
			return geocoder.getFromLocation(latitude, longitude, 1);	
		} catch (IOException e) {
			throw new GeocodingException(e.getMessage());
		}
	}



	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override // Handle the tap event from the touchpad.
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// Handle tap events.
		case KeyEvent.KEYCODE_DPAD_CENTER:
		case KeyEvent.KEYCODE_ENTER:
			openOptionsMenu();
			return true;
		default:
			return super.onKeyDown(keyCode, event);
		}


	}

	private GestureDetector createGestureDetector(Context context) {
		Log.d(TAG, "Creating Gesture Detector");
		GestureDetector gestureDetector = new GestureDetector(context);
		//Create a base listener for generic gestures
		gestureDetector.setBaseListener( new GestureDetector.BaseListener() {
			@Override
			public boolean onGesture(Gesture gesture) {
				if (gesture == Gesture.TAP) {
					// do something on tap
					Log.v(TAG,"tap");
					//if (readyForMenu) {
					TextView tap = (TextView) findViewById(R.id.tap_instruction);
					tap.setVisibility(View.INVISIBLE);

					openOptionsMenu();
					//}
					return true;
				} else if (gesture == Gesture.TWO_TAP) {
					// do something on two finger tap
					return true;
				} else if (gesture == Gesture.SWIPE_RIGHT) {
					// do something on right (forward) swipe
					return true;
				} else if (gesture == Gesture.SWIPE_LEFT) {
					// do something on left (backwards) swipe
					return true;
				}  
				return false;
			}
		});
		gestureDetector.setFingerListener(new GestureDetector.FingerListener() {
			@Override
			public void onFingerCountChanged(int previousCount, int currentCount) {
				// do something on finger count changes
			}
		});
		gestureDetector.setScrollListener(new GestureDetector.ScrollListener() {
			@Override
			public boolean onScroll(float displacement, float delta, float velocity) {
				return false;
			}
		});


		return gestureDetector;
	}



	private void displaySpeechRecognizer(int requestCode) {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		Log.v(TAG, "Inbound Request Code: " + requestCode);
		String prompt = "Speak your message";
		switch (requestCode) {
		case NAME_REQUEST_CODE:
			if (StringUtils.isNotBlank(contactName)) {
				prompt = "Name is set to \n" + contactName + "\nsay 'cancel' to keep or speak a new name to update";
			} else {
				prompt = "Speak the contacts name";
			}
			break;
		case PHONE_REQUEST_CODE:
			if (StringUtils.isNotBlank(phoneNumber)) {
				prompt = "Phone number is set to \n" + phoneNumber + "\nsay 'cancel' to keep or speak a new phone number to update";
			} else {
				prompt = "Speak the phone number";
			}
			break;
		case EMAIL_REQUEST_CODE:
			if (StringUtils.isNotBlank(emailAddress)) {
				prompt = "Email address is \n" + emailAddress + "\nsay 'cancel' to keep or speak a new email address to update";
			} else {
				prompt = "Speak the email address";
			}
			break;
		case ADDRESS_REQUEST_CODE:
			if (StringUtils.isNotBlank(emailAddress)) {
				prompt = "Address is set to \n" + emailAddress + "\nsay 'cancel' to keep or speak a new email address to update";
			} else {
				prompt = "Speak the address";
			}
			break;
		default :
			break;
		}
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT, prompt);
		Log.v(TAG, "Request Code: " + requestCode + " Prompt: " + prompt);
		startActivityForResult(intent, requestCode);

	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		TextView tap = (TextView) findViewById(R.id.tap_instruction);

		switch (item.getItemId()) {
		case R.id.retry:
			Log.d(TAG, "Selected 'retry'");
			Intent intent = getIntent();
			finish();
			startActivity(intent);
			return true;
		case R.id.next:
			Log.d(TAG, "Selected 'next'");
			return true;
		case R.id.cancel:
			Log.d(TAG, "Selected 'cancel'");
			finish();
			return true;
		case R.id.view:
			Log.d(TAG, "Selected 'view'");
			tap.setText("tap to return to menu");
			tap.setVisibility(View.VISIBLE);
			return true;
		case R.id.name:
			this.displaySpeechRecognizer(NAME_REQUEST_CODE);
			break;
		case R.id.phone:
			this.displaySpeechRecognizer(PHONE_REQUEST_CODE);
			break;
		case R.id.email:
			this.displaySpeechRecognizer(EMAIL_REQUEST_CODE);
			break;
		case R.id.address:
			tap.setText("finding current location...");
			tap.setVisibility(View.VISIBLE);
			address = findCurrentLocationAddress();
			this.displaySpeechRecognizer(ADDRESS_REQUEST_CODE);
			break;
		default:
			return super.onOptionsItemSelected(item); 
		}
		
		return super.onOptionsItemSelected(item); 
		
	}

	private String findCurrentLocationAddress() throws GeocodingException {

		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		boolean gpsIsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		Log.i(getLocalClassName(), "GPS is enabled: " + gpsIsEnabled);

		boolean networkIsEnabled = locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		Log.i(getLocalClassName(), "Network is enabled: " + networkIsEnabled);

		final Location location = locationManager.getLastKnownLocation(gpsIsEnabled ? LocationManager.GPS_PROVIDER : LocationManager.NETWORK_PROVIDER);

		// Convert to address
		final StringBuilder buf = new StringBuilder();
		final List<Address> addresses =  geocode(location.getLatitude(), location.getLongitude());

		if (addresses.isEmpty()) {
			throw new GeocodingException("Could not find location (no geolocation data returned)");
		}

		final String address = addresses.get(0).getAddressLine(0);
		final String city = addresses.get(0).getAddressLine(1);
		//final String country = addresses.get(0).getAddressLine(2);


		if (StringUtils.isNotBlank(address)) {
			buf.append(address);
		}

		if (StringUtils.isNotBlank(address) && StringUtils.isNotBlank(city)) {
			buf.append(" ");
		}

		if (StringUtils.isNotBlank(city)) {
			buf.append(city);
		}

		if (buf.length() <= 0) {
			throw new GeocodingException("Could not find location (address data contained no address or city)");
		}

		return buf.toString();
	}

	private String contactName;

	private String phoneNumber;

	private String emailAddress;

	private String address;

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

		picTaken = true;
		switch(requestCode) {
		case (TAKE_PHOTO_REQUEST_CODE) : {
			if (resultCode == Activity.RESULT_OK) {
				// TODO Extract the data returned from the child Activity.
				Log.v(TAG,"onActivityResult"); 

				File f = new File(IMAGE_FILE_NAME);
				if (f.exists()) {
					Log.v(TAG,"image file from camera was found");
					Bitmap b = BitmapFactory.decodeFile(IMAGE_FILE_NAME);
					Log.v(TAG,"bmp width=" + b.getWidth() + " height=" + b.getHeight());
					ImageView image = (ImageView) findViewById(R.id.bgPhoto);
					image.setImageBitmap(b);
					openOptionsMenu();
				}
			} else {
				Log.v(TAG,"onActivityResult returned bad result code");
				finish();
			}
			break;
		} 
		case (NAME_REQUEST_CODE) : {
			if (resultCode == RESULT_OK) {
				List<String> results = intent.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				if (!results.get(0).equalsIgnoreCase("cancel")) {
					contactName = results.get(0);
				}
				Log.v(TAG, "Contact Name: " + contactName);
			}
			
			break;
		}
		case (EMAIL_REQUEST_CODE) : {
			if (resultCode == RESULT_OK) {
				List<String> results = intent.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				if (!results.get(0).equalsIgnoreCase("cancel")) {
					emailAddress = results.get(0);
				}
				Log.v(TAG, "Contact Email Address: " + emailAddress);
			}
			break;
		}
		case (PHONE_REQUEST_CODE) : {
			if (resultCode == RESULT_OK) {
				List<String> results = intent.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				if (!results.get(0).equalsIgnoreCase("cancel")) {
					phoneNumber = results.get(0);
				}
				Log.v(TAG, "Contact Phone Number: " + phoneNumber);
			}
			break;
		}
		case (ADDRESS_REQUEST_CODE) : {
			if (resultCode == RESULT_OK) {
				List<String> results = intent.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				if (!results.get(0).equalsIgnoreCase("ok")) {
					address = results.get(0);
				}
				Log.v(TAG, "Address: " + address);
			}
			break;
		}
		default:
			break;
		}
	}

	@Override
	protected void onDestroy() {
		//Close the Text to Speech Library
		if(mSpeech != null) {
			mSpeech.stop();
			mSpeech.shutdown();
			mSpeech = null;
			Log.d(TAG, "TTS Destroyed");
		}
		super.onDestroy();
	}

}