package com.dugbel.glass.util;

import java.util.Arrays;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class HeadGestureDetector {
	
	/**
     * 
     * @author dbell
     *
     */
    private class HeadGestureSensorEventListener implements SensorEventListener {

    	/*
    	 * (non-Javadoc)
    	 * @see android.hardware.SensorEventListener#onAccuracyChanged(android.hardware.Sensor, int)
    	 */
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        	// TODO Auto-generated method stub
        }

        /*
         * (non-Javadoc)
         * @see android.hardware.SensorEventListener#onSensorChanged(android.hardware.SensorEvent)
         */
        public void onSensorChanged(SensorEvent event) {

            if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
                Log.w(this.getClass().getSimpleName(), "Unreliable event...");
            }

            int sensorType = event.sensor.getType();

            if (sensorType == Sensor.TYPE_GYROSCOPE) {
                if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
                    Log.w(this.getClass().getSimpleName(), "Unreliable gyroscope event...");
                    // return;
                }

                orientationVelocity = event.values.clone();

                // state timeout check
                if (event.timestamp - lastStateChanged > STATE_TIMEOUT_NSEC && state != State.IDLE) {
                    Log.d(this.getClass().getSimpleName(), "state timeouted");
                    lastStateChanged = event.timestamp;
                    state = State.IDLE;
                }

                Log.d(this.getClass().getSimpleName(), "V:" + Arrays.toString(orientationVelocity));

                // check if glass is put on
                int maxVelocityIndex = maxAbsIndex(orientationVelocity);
                if (isStable(orientationVelocity)) {
                    Log.d(this.getClass().getSimpleName(), "isStable");
                } else if (maxVelocityIndex == 0) {
                    if (orientationVelocity[0] < -MIN_MOVE_ANGULAR_VELOCITY) {
                        if (state == State.IDLE) {
                            Log.d(this.getClass().getSimpleName(), "isNod");
                            state = State.GO_DOWN;
                            lastStateChanged = event.timestamp;
                            if (listener != null) {
                                listener.onNod();
                            }
                        }
                    }
                } else if (maxVelocityIndex == 1) {
                    if (orientationVelocity[1] < -MIN_MOVE_ANGULAR_VELOCITY) {
                        if (state == State.IDLE) {
                            Log.d(this.getClass().getSimpleName(), "V:" + Arrays.toString(orientationVelocity));
                            state = State.SHAKE_TO_RIGHT;
                            lastStateChanged = event.timestamp;
                            if (listener != null) {
                                listener.onShakeToRight();
                            }
                        }
                    } else if (orientationVelocity[1] > MIN_MOVE_ANGULAR_VELOCITY) {
                        if (state == State.IDLE) {
                            Log.d(this.getClass().getSimpleName(), "V:" + Arrays.toString(orientationVelocity));
                            state = State.SHAKE_TO_LEFT;
                            lastStateChanged = event.timestamp;
                            if (listener != null) {
                                listener.onShakeToLeft();
                            }
                        }
                    }
                }
            }
        }
    }
   
    /**
     * 
     * @author dbell
     *
     */
    public interface OnHeadGestureListener {
        
    	/**
    	 * 
    	 */
    	public void onNod();

    	/**
    	 * 
    	 */
        public void onShakeToLeft();

        /**
         * 
         */
        public void onShakeToRight();
    }
    
    /** */
    private static enum State {
        BACK_DOWN, BACK_UP, GO_DOWN, GO_UP, IDLE, SHAKE_BACK_TO_LEFT, SHAKE_BACK_TO_RIGHT, SHAKE_TO_LEFT, SHAKE_TO_RIGHT
    }

    /** */
    private static final float MIN_MOVE_ANGULAR_VELOCITY = 1.00F;
    
    /** */
    private static final int[] REQUIRED_SENSORS = { Sensor.TYPE_GYROSCOPE };
    
    /** */
    private static final int[] SENSOR_RATES = { SensorManager.SENSOR_DELAY_NORMAL };
    
    /** */
    private static final float STABLE_ANGULAR_VELOCITY = 0.10F;

    /** */
    private static final long STATE_TIMEOUT_NSEC = 1000 * 1000 * 1000;

    /**
     * 
     * @param orientationVelocity
     * @return
     */
    private static boolean isStable(float[] orientationVelocity) {
        if (Math.abs(orientationVelocity[0]) < STABLE_ANGULAR_VELOCITY
                && Math.abs(orientationVelocity[1]) < STABLE_ANGULAR_VELOCITY
                && Math.abs(orientationVelocity[2]) < STABLE_ANGULAR_VELOCITY) {
            return true;
        }
        return false;
    }
    
    /**
     * 
     * @param array
     * @return
     */
    private static int maxAbsIndex(float[] array) {
        int n = array.length;
        float maxValue = Float.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < n; i++) {
            float val = Math.abs(array[i]);
            if (val > maxValue) {
                maxValue = val;
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    /** */
    private long lastStateChanged = -1;

    /** */
    private OnHeadGestureListener listener;

    /** */
    private float[] orientationVelocity = new float[3];

    /** */
    private final SensorEventListener sensorEventListener;

    /** */
    private final SensorManager sensorManager;

    /** */
    private State state = State.IDLE;

    /**
     * 
     * @param context
     */
    public HeadGestureDetector(Context context) {
    	 sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        sensorEventListener = new HeadGestureSensorEventListener();
    }

    /**
     * 
     * @param listener
     */
    public void setOnHeadGestureListener(OnHeadGestureListener listener) {
        this.listener = listener;
    }

    /**
     * 
     */
    public void start() {
        for (int i = 0; i < REQUIRED_SENSORS.length; i++) {
            Sensor sensor = sensorManager.getDefaultSensor(REQUIRED_SENSORS[i]);
            if (sensor != null) {
                Log.d(this.getClass().getSimpleName(), "registered:" + sensor.getName());
                sensorManager.registerListener(sensorEventListener, sensor, SENSOR_RATES[i]);
            }
        }
    }

    /**
     * 
     */
    public void stop() {
        sensorManager.unregisterListener(sensorEventListener);
    }
}