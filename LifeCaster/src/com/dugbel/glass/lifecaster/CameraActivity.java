package com.dugbel.glass.lifecaster;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;

public class CameraActivity extends Activity implements SurfaceHolder.Callback {
	
	public static final int BUFFER_SIZE = 1024 * 8;
	
	private static final String TAG = CameraActivity.class.getSimpleName();
	
	//a bitmap to display the captured image
	private Bitmap bmp;
	
	String imageFileName = "";
	
	//a variable to store a reference to the Image View at the main.xml file.
	private ImageView iv_image;
	
	//a variable to control the camera
	private Camera mCamera;
	
	//the camera parameters
	private Parameters parameters;
	
	//a surface holder
	private SurfaceHolder sHolder;


	//a variable to store a reference to the Surface View at the main.xml file
	private SurfaceView sv;

	/**
	 * 
	 * @param data
	 * @param reqWidth
	 * @param reqHeight
	 * @return
	 */
	public static Bitmap decodeSampledBitmapFromData(byte[] data,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(data, 0, data.length,options);
		options.inSampleSize = 2; // saved image will be one half the width and height of the original (image captured is double the resolution of the screen size)
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeByteArray(data, 0, data.length,options);
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.v(TAG,"onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);
		//get the Image View at the main.xml file
		iv_image = (ImageView) findViewById(R.id.imageView);
		sv = (SurfaceView) findViewById(R.id.surfaceView);
		//Get a surface
		sHolder = sv.getHolder();
		sHolder.addCallback(this);
		Bundle extras = getIntent().getExtras();
		// get the image file name from the caller to save the 640x360 image
		imageFileName = extras.getString("imageFileName");
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	public void onDestroy()
	{
		Log.v(TAG,"onDestroy");
		super.onDestroy();

		if (mCamera != null) {
			mCamera.stopPreview();
			//release the camera
			mCamera.release();
			//unbind the camera from this object
			mCamera = null;
		}
	}


	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onPause()
	 */
	@Override
	public void onPause()
	{
		Log.v(TAG,"onPause");
		super.onPause();
		if (mCamera != null) {
			mCamera.stopPreview();
			//release the camera
			mCamera.release();
			//unbind the camera from this object
			mCamera = null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.SurfaceHolder.Callback#surfaceChanged(android.view.SurfaceHolder, int, int, int)
	 */
	@Override
	public void surfaceChanged(SurfaceHolder surfaceHolder, int arg1, int arg2, int arg3)
	{
		Log.v(TAG,"surfaceChanged");
		//get camera parameters
		try {
			parameters = mCamera.getParameters();
			Log.v(TAG,"got parms");
			//set camera parameters
			parameters.setPreviewSize(640,360);
			parameters.setPictureSize(1280,720);
			//Camera.Parameters params = mCamera.getParameters();
			parameters.setPreviewFpsRange(30000, 30000);
			Log.v(TAG,"parms were set");
			mCamera.setParameters(parameters);

			mCamera.startPreview();

			// TODO TESTING
			//  //mCamera.startFaceDetection();

			//          mCamera.unlock();
			//          
			//          MediaRecorder recorder = new MediaRecorder();
			//          recorder.setCamera(mCamera);
			//          recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			//          recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
			//         // //recorder.setVideoSize(640, 360);
			//          recorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);
			//          recorder.setOutputFile(Environment.getExternalStorageDirectory().getPath() + "/Pictures/addcontact_preview.mp4");
			//          recorder.setPreviewDisplay(surfaceHolder.getSurface());
			//          recorder.setMaxDuration(3000);
			//
			//          recorder.prepare();
			//          recorder.start();
			//          
			//         
			//         
			//          recorder.stop();
			//          recorder.reset();   // You can reuse the object by going back to setAudioSource() step
			//          recorder.release(); // Now the object cannot be reused
			//          
			//          mCamera.reconnect();
			//TODO TESTING

			Log.v(TAG,"preview started");

			//sets what code should be executed after the picture is taken
			Camera.PictureCallback mCall = new Camera.PictureCallback()
			{
				public void onPictureTaken(byte[] data, Camera camera)
				{
					Log.v(TAG,"pictureTaken");
					Log.v(TAG,"data bytes=" + data.length);
					//decode the data obtained by the camera into a Bitmap
					Bitmap bmp = decodeSampledBitmapFromData(data,640,360);
					Log.v(TAG,"bmp width=" + bmp.getWidth() + " height=" + bmp.getHeight());
					FileOutputStream outStream = null;
					try{
						FileOutputStream fos = new FileOutputStream(imageFileName);
						final BufferedOutputStream bos = new BufferedOutputStream(fos, BUFFER_SIZE);
						bmp.compress(CompressFormat.JPEG, 100, bos);
						bos.flush();
						bos.close();
						fos.close();
					} catch (FileNotFoundException e){
						Log.v(TAG, e.getMessage());
					} catch (IOException e){
						Log.v(TAG, e.getMessage());
					}
					Intent resultIntent = new Intent();
					// TODO Add extras or a data URI to this intent as appropriate.
					resultIntent.putExtra("testString","here is my test");
					setResult(Activity.RESULT_OK, resultIntent);
					finish();
				}
			};
			Log.v(TAG,"set callback");

			mCamera.takePicture(null, null, mCall);
		}       
		catch (Exception e) {
			try {
				mCamera.release();
				Log.e(TAG,"released the camera");
			}
			catch (Exception ee) {
				// do nothing
				Log.e(TAG,"error releasing camera");
				Log.e(TAG,"Exception encountered relerasing camera, exiting:" + ee.getLocalizedMessage());
			}
			Log.e(TAG,"Exception encountered, exiting:" + e.getLocalizedMessage());
			mCamera = null;  
			Intent resultIntent = new Intent();
			setResult(Activity.RESULT_CANCELED, resultIntent);
			finish();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.view.SurfaceHolder.Callback#surfaceCreated(android.view.SurfaceHolder)
	 */
	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		Log.v(TAG,"surfaceCreated");
		// The Surface has been created, acquire the camera and tell it where
		// to draw the preview.
		try {
			mCamera = Camera.open();
			Log.v(TAG,"acquired the camera");
			mCamera.setPreviewDisplay(holder);
			Log.v(TAG,"set surface holder for preview");
		}
		catch (Exception e) {
			try {
				mCamera.release();
				Log.v(TAG,"released the camera");
			}
			catch (Exception ee) {
				// do nothing
				Log.e(TAG,"Exception encountered releasing camera, exiting:" + ee.getLocalizedMessage());
			}
			Log.e(TAG,"Exception encountered, exiting:" + e.getLocalizedMessage());
			mCamera = null;  
			Intent resultIntent = new Intent();
			setResult(Activity.RESULT_CANCELED, resultIntent);
			finish();
		}

	}

	/*
	 * (non-Javadoc)
	 * @see android.view.SurfaceHolder.Callback#surfaceDestroyed(android.view.SurfaceHolder)
	 */
	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		Log.v(TAG,"surfaceDestroyed");
		if (mCamera != null) {
			mCamera.stopPreview();
			//release the camera
			mCamera.release();
			//unbind the camera from this object
			mCamera = null;
		}  
	}
}