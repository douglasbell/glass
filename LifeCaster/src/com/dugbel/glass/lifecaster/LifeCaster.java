package com.dugbel.glass.lifecaster;

import android.os.SystemClock;

/**
 * Model holding the LifeCaster state.
 */
public class LifeCaster {

    /**
     * Interface to listen for changes on the {@link LifeCaster}.
     */
    public interface LifeCasterListener {
        /** LifeCaster has started. */
        public void onStart();
        /** LifeCaster has been paused. */
        public void onPause();
        /** LifeCaster has been reset */
        public void onReset();
    }

    private long mDurationMillis;
    private long mStartTimeMillis;
    private long mPauseTimeMillis;

    private LifeCasterListener mListener;

    public LifeCaster() {
        this(0);
    }

    public LifeCaster(long durationMillis) {
        setDurationMillis(durationMillis);
    }

    /**
     * Sets the timer's duration in milliseconds.
     */
    public void setDurationMillis(long durationMillis) {
        mDurationMillis = durationMillis;
        if (mListener != null) {
            mListener.onReset();
        }
    }

    /**
     * Gets the timer's duration in milliseconds.
     */
    public long getDurationMillis() {
        return mDurationMillis;
    }

    /**
     * Returns whether or not the timer is running.
     */
    public boolean isRunning() {
        return mStartTimeMillis > 0 && mPauseTimeMillis == 0;
    }

    /**
     * Returns whether or not the timer has been started.
     */
    public boolean isStarted() {
        return mStartTimeMillis > 0;
    }

    /**
     * Gets the remaining time in milliseconds.
     */
    public long getRemainingTimeMillis() {
        long remainingTime = mDurationMillis;

        if (mPauseTimeMillis != 0) {
            remainingTime -= mPauseTimeMillis - mStartTimeMillis;
        } else if (mStartTimeMillis != 0) {
            remainingTime -= SystemClock.elapsedRealtime() - mStartTimeMillis;
        }

        return remainingTime;
    }

    /**
     * Starts the timer.
     */
    public void start() {
        long elapsedTime = mPauseTimeMillis - mStartTimeMillis;

        mStartTimeMillis = SystemClock.elapsedRealtime() - elapsedTime;
        mPauseTimeMillis = 0;
        if (mListener != null) {
            mListener.onStart();
        }
    }

    /**
     * Pauses the timer.
     */
    public void pause() {
        if (isStarted()) {
            mPauseTimeMillis = SystemClock.elapsedRealtime();
            if (mListener != null) {
                mListener.onPause();
            }
        }
    }

    /**
     * Resets the timer.
     */
    public void reset() {
        mStartTimeMillis = 0;
        mPauseTimeMillis = 0;
        if (mListener != null) {
            mListener.onPause();
            mListener.onReset();
        }
    }

    /**
     * Sets a {@link LifeCasterListener}.
     */
    public void setListener(LifeCasterListener listener) {
        mListener = listener;
    }
}
