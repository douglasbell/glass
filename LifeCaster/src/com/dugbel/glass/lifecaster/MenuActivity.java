package com.dugbel.glass.lifecaster;


import java.io.IOException;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.dugbel.glass.exception.GeocodingException;

/**
 * Activity showing the options menu.
 */
public class MenuActivity extends Activity {

	private static final String TAG = "MenuActivity";
	
    /** Request code for setting the timer. */
    private static final int SET_TIMER = 100;

    private LifeCaster mTimer;
    private boolean mResumed;
    private boolean mSettingTimer;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (service instanceof LifeCasterService.TimerBinder) {
                mTimer = ((LifeCasterService.TimerBinder) service).getTimer();
                openOptionsMenu();
            }
            // No need to keep the service bound.
            unbindService(this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            // Nothing to do here.
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindService(new Intent(this, LifeCasterService.class), mConnection, 0);
    }

    @Override
    public void onResume() {
        super.onResume();
        mResumed = true;
        openOptionsMenu();
    }

    @Override
    public void onPause() {
        super.onPause();
        mResumed = false;
    }

    @Override
    public void openOptionsMenu() {
        if (mResumed && mTimer != null) {
            super.openOptionsMenu();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.timer, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final boolean timeSet = mTimer.getDurationMillis() != 0;

        menu.setGroupVisible(R.id.no_time_set, !timeSet);
        menu.setGroupVisible(R.id.time_set, timeSet);
        if (timeSet) {
            menu.findItem(R.id.start).setVisible(!mTimer.isRunning() && !mTimer.isStarted());
            menu.findItem(R.id.resume).setVisible(!mTimer.isRunning() && mTimer.isStarted());
            menu.findItem(R.id.pause).setVisible(
                    mTimer.isRunning() && mTimer.getRemainingTimeMillis() > 0);
            menu.findItem(R.id.reset).setVisible(mTimer.isStarted());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection.
        switch (item.getItemId()) {
            case R.id.change_timer:
            case R.id.set_timer:
                mTimer.reset();
                setTimer();
                return true;
            case R.id.start:
            case R.id.resume:
                mTimer.start();
                return true;
            case R.id.pause:
                mTimer.pause();
                return true;
            case R.id.reset:
                mTimer.reset();
                return true;
            case R.id.stop:
                stopService(new Intent(this, LifeCasterService.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        if (!mSettingTimer) {
            // Nothing else to do, closing the Activity.
            finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == SET_TIMER) {
            mTimer.setDurationMillis(data.getLongExtra(LifeCasterActivity.EXTRA_DURATION_MILLIS, 0));
        }
        finish();
    }

    private void setTimer() {
        Intent setTimerIntent = new Intent(this, LifeCasterActivity.class);

        setTimerIntent.putExtra(LifeCasterActivity.EXTRA_DURATION_MILLIS, mTimer.getDurationMillis());
        startActivityForResult(setTimerIntent, SET_TIMER);
        mSettingTimer = true;
    }
    
    
    /**
	 * 
	 * @param latitude
	 * @param longitude
	 * @return
	 * @throws GeocodingException
	 */
	private List<Address> geocode(final double latitude, final double longitude) 
			throws GeocodingException {
		try {
			Geocoder geocoder = new Geocoder(this, Locale.getDefault());
			return geocoder.getFromLocation(latitude, longitude, 1);	
		} catch (IOException e) {
			throw new GeocodingException(e.getMessage());
		}
	}

	
	private String getCurrentAddress() throws GeocodingException {

		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		boolean gpsIsEnabled = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		Log.v(TAG, "GPS is enabled: " + gpsIsEnabled);

		boolean networkIsEnabled = locationManager
				.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

		Log.v(TAG, "Network is enabled: " + networkIsEnabled);

		final Location location = locationManager.getLastKnownLocation(gpsIsEnabled ? LocationManager.GPS_PROVIDER : LocationManager.NETWORK_PROVIDER);

		
		final long fix = location.getTime(); // UTC time of the last fix
		final DateTime fixDateTime = new DateTime(fix);
		if (fixDateTime.isBefore(new DateTime().minusHours(1))) {
			throw new GeocodingException("Fix time is out of date. Last fix: " + fixDateTime.toString("yyyy-MM-dd hh:mm:ss"));
		}
		
		// Convert to address
		final StringBuilder buf = new StringBuilder();
		final List<Address> addresses =  geocode(location.getLatitude(), location.getLongitude());

		if (addresses.isEmpty()) {
			throw new GeocodingException("Could not find location (no geolocation data returned)");
		}

		final String address = addresses.get(0).getAddressLine(0);
		final String city = addresses.get(0).getAddressLine(1);
		final String country = addresses.get(0).getAddressLine(2);


		if (StringUtils.isNotBlank(address)) {
			buf.append(address);
		}

		if (StringUtils.isNotBlank(address) && StringUtils.isNotBlank(city)) {
			buf.append(" ");
		}

		if (StringUtils.isNotBlank(city)) {
			buf.append(city);
		}
		
		if (StringUtils.isNotBlank(city) && StringUtils.isNotBlank(country)) {
			buf.append(" ");
		}
		
		if (StringUtils.isNotBlank(country)) {
			buf.append(country);
		}

		if (buf.length() <= 0) {
			throw new GeocodingException("Could not find location (address data contained no address or city)");
		}

		return buf.toString();
	}
}
