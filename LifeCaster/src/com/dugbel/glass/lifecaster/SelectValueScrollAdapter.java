package com.dugbel.glass.lifecaster;

import com.google.android.glass.widget.CardScrollAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

/**
 * Adapter for the {@link CardSrollView} inside {@link SelectValueActivity}.
 */
public class SelectValueScrollAdapter extends CardScrollAdapter {

    private final Context mContext;
    private final int mCount;

    public SelectValueScrollAdapter(Context context, int count) {
        mContext = context;
        mCount = count;
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.card_select_value, parent);
        }

        final TextView view = (TextView) convertView.findViewById(R.id.value);
        view.setText(String.format("%02d", position));

        return setItemOnCard(this, convertView);
    }

    @Override
    public int findIdPosition(Object id) {
        if (id instanceof Integer) {
            int idInt = (Integer) id;
            if (idInt >= 0 && idInt < mCount) {
                return idInt;
            }
        }
        return AdapterView.INVALID_POSITION;
    }

    @Override
    public int findItemPosition(Object item) {
        return findIdPosition(item);
    }
}
