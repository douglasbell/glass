package com.dugbel.glass.pricethis;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang.StringUtils;
import org.joda.time.format.ISODateTimeFormat;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Process;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.aws.util.SignedRequestsHelper;
import com.dugbel.glass.exception.InitializationException;
import com.mirasense.scanditsdk.interfaces.ScanditSDK;

/**
 * {@link Activity} for looking up and presenting the scanned items title, 
 * description and price points
 * 
 * @author Doug Bell (douglas.bell@gmail.com)
 *
 */
public class ItemLookupActivity extends Activity {

	/** AWS Search ID Type */
	public static enum IdType {EAN, ISBN, SKU, UPC};
	
	/** Scandit Symbology */
	//public static enum Symbology {EAN8, EAN13, UPC12, UPCE, CODE128, GS1-128, CODE39, ITF, QR, GS1-QR, DATAMATRIX, GS1-DATAMATRIX, PDF417, MSI};

	/**
	 * {@link AsyncTask} that makes the call to the AWS Item Search Service
	 * then reads and converts the returned document into a {@link ItemLookupResponse}
	 * finally updating and displaying the static card with the item information.
	 * 
	 * @author Doug Bell (douglas.bell@gmail.com)
	 *
	 */
	class ItemLookupTask extends AsyncTask<String, Integer, ItemLookupResponse> {

		/** Amazon associate ID */
		private static final String AMAZON_ASSOCIATE_ID = "glass00e-20";

		/**  AWS Access Key ID */
		private static final String AWS_ACCESS_KEY_ID = "AKIAJWHTZQZ4WA36IXZA";

		/** AWS Secret Key */
		private static final String AWS_SECRET_KEY = "KHjWJllY/E9LMgWSWI53ac6EvlMD+co78PscsSQp";

		/**
		 * Use one of the following end-points for the requested region
		 * 
		 *      US: ecs.amazonaws.com 
		 *      CA: ecs.amazonaws.ca 
		 *      UK: ecs.amazonaws.co.uk 
		 *      DE: ecs.amazonaws.de 
		 *      FR: ecs.amazonaws.fr 
		 *      JP: ecs.amazonaws.jp
		 * 
		 */
		private static final String ENDPOINT = "ecs.amazonaws.com";

		/** The {@link Log} tag */
		private static final String TAG = "ItemLookup";

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		protected ItemLookupResponse doInBackground(String... params) {

			// Give the task a bit more horsepower for the DOM parsing
			Process.setThreadPriority(Process.THREAD_PRIORITY_FOREGROUND);

			if (params.length != 2) {
				throw new RuntimeException("Expected two params received " + params.length);
			}

			final String upc = params[0];
			final String idType = params[1];

			Log.d(TAG, "lookup starting. UPC " + upc + " idType " + idType);

			// Create the parameter map
			final Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("Service", "AWSECommerceService");
			paramMap.put("Timestamp", ISODateTimeFormat.basicDateTime().toString());
			paramMap.put("Operation", "ItemLookup");
			paramMap.put("IdType", idType);
			paramMap.put("ItemId", upc);
			paramMap.put("SearchIndex", "All");
			paramMap.put("ResponseGroup", "ItemAttributes,Images,OfferSummary,EditorialReview");
			paramMap.put("AssociateTag" ,  AMAZON_ASSOCIATE_ID);
			paramMap.put("IncludeReviewsSummary", "flase");

			// Set up the signed requests helper 
			final SignedRequestsHelper helper;
			try {
				helper = SignedRequestsHelper.getInstance(ENDPOINT, AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
				return response;
			}

			// Create the signed request URL
			Log.d(TAG, "Signing request");

			final String requestUrl = helper.sign(paramMap);
			Log.d(TAG, "Signed request URL: " + requestUrl);

			try {
				Log.d(TAG, "Retrieving and parsing document");


				final Document doc = builder.parse(requestUrl);

				Log.d(TAG, "Parsing specific nodes via xpath");

				final XPathExpression imgSrcExp = xpath.compile("/ItemLookupResponse/Items/Item/MediumImage/URL/text()");
				final String imgSrc = (String)imgSrcExp.evaluate(doc, XPathConstants.STRING);
				response.setImgSrc(imgSrc);

				final XPathExpression descriptionExp = xpath.compile("/ItemLookupResponse/Items/Item[1]/EditorialReviews/EditorialReview/Content/text()");
				final String description = (String)descriptionExp.evaluate(doc, XPathConstants.STRING);
				response.setDescription(description);

				final XPathExpression titleExp = xpath.compile("/ItemLookupResponse/Items/Item/ItemAttributes/Title/text()");
				final String title = (String)titleExp.evaluate(doc, XPathConstants.STRING);
				response.setTitle(title);		

				final XPathExpression lowestNewPriceExp = xpath.compile("/ItemLookupResponse/Items/Item/OfferSummary/LowestNewPrice/FormattedPrice/text()");
				final String lowestNewPrice = (String)lowestNewPriceExp.evaluate(doc, XPathConstants.STRING);
				response.setLowestNewPrice(lowestNewPrice);

				final XPathExpression lowestUsedPriceExp = xpath.compile("/ItemLookupResponse/Items/Item/OfferSummary/LowestUsedPrice/FormattedPrice/text()");
				final String lowestUsedPrice = (String)lowestUsedPriceExp.evaluate(doc, XPathConstants.STRING);
				response.setLowestUsedPrice(lowestUsedPrice);

			} catch (IOException e) { 
				Log.e(TAG, e.getMessage()); 
			} catch (XPathExpressionException e) { 
				Log.e(TAG, e.getMessage()); 
			} catch (SAXException e) { 
				Log.e(TAG, e.getMessage()); 
			}

			Log.d(TAG, "Returning response");

			return response;
		}

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(ItemLookupResponse response) {
			Log.d(TAG, "onPostExecute start");

			setContentView(R.layout.activity_item_lookup);

			final TextView productNameTextView = (TextView) findViewById(R.id.productNameTextView);
			if (StringUtils.isNotBlank(response.getDescription())) {
				productNameTextView.setText(response.getTitle());
			}

			final TextView newPriceTextView = (TextView) findViewById(R.id.newPriceTextView);
			if (StringUtils.isNotBlank(response.getLowestNewPrice())) {
				newPriceTextView.setText(response.getLowestNewPrice());
			} 

			final TextView usedPriceTextView = (TextView) findViewById(R.id.usedPriceTextView);
			if (StringUtils.isNotBlank(response.getLowestUsedPrice())) {
				usedPriceTextView.setText(response.getLowestUsedPrice());
			}

			final TextView descriptionTextView = (TextView) findViewById(R.id.descriptionTextView);
			if (StringUtils.isNotBlank(response.getDescription())) {
				if (response.getDescription().length() > 200) {
					String substr = StringUtils.substring(response.getDescription(), 0, 200);
					String description = (StringUtils.substringBeforeLast(substr, " ") + "...").replaceAll("<.+?>", "").replaceAll("\\n|\\r", " ");
					descriptionTextView.setText(description);
				}
			}

			final ImageView iv = (ImageView) findViewById(R.id.productImageView); 
			try {
				StrictMode.setThreadPolicy(ThreadPolicy.LAX);
				final Bitmap productBitmap = BitmapFactory.decodeStream((InputStream)new URL(response.getImgSrc()).getContent());
				response.setProductBitmap(productBitmap);
				iv.setImageBitmap(productBitmap);
			} catch (MalformedURLException e) {
				Log.e(TAG, e.getMessage() != null ? e.getMessage() : "Malformed URL. (" + response.getImgSrc() + ")");
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
			}

			Log.d(TAG, "onPostExecute Complete");
		}

	};

	/** Instance of {@link DocumentBuilder} */
	private static final DocumentBuilder builder;

	/** The {@link Log} tag */
	private static final String TAG = "ItemLookupActivity";

	/** Instnace of {@link XPath} */
	private static final XPath xpath = XPathFactory.newInstance().newXPath();

	/** Initialize the {@link DocumentBuilder} */
	static{
		try {
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

			// No need to validate we know the XML is valid
			factory.setNamespaceAware(false);
			factory.setValidating(false);

			builder = factory.newDocumentBuilder();
			builder.setEntityResolver(new EntityResolver() {
				@Override
				public InputSource resolveEntity(String publicId, String systemId)
						throws SAXException, IOException {
					return new InputSource(new StringReader(""));

				}
			});

		} catch (ParserConfigurationException e) {
			throw new InitializationException(e.getMessage());
		}
	}

	/** The {@link ItemLookupResponse} holder */
	private ItemLookupResponse response = new ItemLookupResponse();

	/**
	 * Turn the symbology requested from the {@link ScanditSDK} to an
	 * {@link IdType} AWS will understand.
	 * 
	 * @param symbology		The {@link ScanditSDK} symbology
	 * 
	 * @return {@link IdType}
	 */
	private IdType fromSymbology(final String symbology) {
		for (IdType idType : IdType.values()) {
			if (symbology.toUpperCase(Locale.US).contains(idType.name())) {
				return idType;
			}
		}
		return IdType.UPC;
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		Log.d(TAG, "onCreate starting");


		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		final String barcode = getIntent().getExtras().getString("barcode");
		final String symbology = getIntent().getExtras().getString("symbology");

		Log.d(TAG, "Item lookup starting");

		final ItemLookupTask itemLookup = new ItemLookupTask();
		final IdType idType = fromSymbology(symbology);
		itemLookup.execute(barcode, idType.name());

		setContentView(R.layout.activity_item_lookup_interstitial);
		
	}


	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.item_lookup, menu);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override 
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
			openOptionsMenu();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.more_information:
			return true;
		case R.id.enlarge_image:
			Log.d(TAG, "Selected 'enlarge image'");		
			final Intent showImageIntent = new Intent(this, ItemLookupShowImageActivity.class);
			final Bitmap bitmap = response.getProductBitmap();
			final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
			showImageIntent.putExtra("bitmapByteArray", byteStream.toByteArray());
			startActivity(showImageIntent);
			return true;
		case R.id.product_detail:
			Log.d(TAG, "Selected 'product detail'");
			final Intent productDetailIntent = new Intent(this, ItemLookupProductDetailActivity.class);
			productDetailIntent.putExtra("description", response.getDescription());
			startActivity(productDetailIntent);
			Log.d(TAG, "Selected 'more information'");
			return true;
		case R.id.save_for_later:
			Log.d(TAG, "Selected 'save to timeline'");
	
			return true;
		case R.id.scan_another:
			Log.d(TAG, "Selected 'scan another'");
			finish();
			final Intent scanIntent = new Intent(this, PriceThisActivity.class);
			startActivity(scanIntent);
			return true;
		default:
			return super.onOptionsItemSelected(item); 
		}

	}
}