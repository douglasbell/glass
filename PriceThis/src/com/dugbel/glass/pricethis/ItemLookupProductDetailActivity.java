package com.dugbel.glass.pricethis;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;

/**
 * {@link Activity} for showing the raw item description
 * 
 * @author Doug Bell (douglas.bell@gmail.com)
 * 
 */
public class ItemLookupProductDetailActivity extends Activity {

	/** The {@link Log} tag */
	private static final String TAG = "ItemLookupProductDetailActivity";

	/** {@link List} of {@link Card} objects */
	private List<Card> cards = new ArrayList<Card>();
	
	/** Instance of {@link CardScrollView} */
	private CardScrollView cardScrollView;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.v(TAG, "onCreate starting");

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		final String description = getIntent().getStringExtra("description");

		createCards(description);

		cardScrollView = new CardScrollView(this);
		cardScrollView.setAdapter(new DescriptionCardScrollAdapter());
		cardScrollView.activate();
		
		setContentView(cardScrollView);

		Log.v(TAG, "onCreate complete");
	}

	/**
	 * Create the {@link Card} objects
	 * 
	 * @param description	The complete description
	 */
	private void createCards(final String description) {
		
		int cardNo = 0;
		int start = 0;
		int end = 200;

		if (StringUtils.isNotBlank(description)) {
			while (start < end) {
				final String substr = StringUtils.substring(description, start, end);

				start = ((cardNo++ > 0) ? end : 0) + StringUtils.substringBeforeLast(substr, " ").length();
				end = (start + 200) <= description.length() ? (start + 200) : description.length();

				final String txt = (StringUtils.substringBeforeLast(substr, " ")).replaceAll("<.+?>", "").replaceAll("\\n|\\r", " ");
				
				final Card card = new Card(this);
				card.setText(txt);
				cards.add(card);
			} 
		}
	}

	/**
	 * Description Card Scroll Adapter {@link CardScrollAdapter} implementation
	 * 
	 * @author Doug Bell (dougals.bell@gmail.com)
	 *
	 */
	private class DescriptionCardScrollAdapter extends CardScrollAdapter {

		/*
		 * (non-Javadoc)
		 * @see com.google.android.glass.widget.CardScrollAdapter#findIdPosition(java.lang.Object)
		 */
		@Override
		public int findIdPosition(Object id) {
			return -1;
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.android.glass.widget.CardScrollAdapter#findItemPosition(java.lang.Object)
		 */
		@Override
		public int findItemPosition(Object item) {
			return cards.indexOf(item);
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.android.glass.widget.CardScrollAdapter#getCount()
		 */
		@Override
		public int getCount() {
			return cards.size();
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.android.glass.widget.CardScrollAdapter#getItem(int)
		 */
		@Override
		public Object getItem(int position) {
			return cards.get(position);
		}

		/*
		 * (non-Javadoc)
		 * @see com.google.android.glass.widget.CardScrollAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			return cards.get(position).toView();
		}
	}
}