package com.dugbel.glass.pricethis;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import android.graphics.Bitmap;

import com.dugbel.glass.pricethis.ItemLookupActivity;

/**
 * Object holder for the {@link ItemLookupActivity} lookup response
 * 
 * @author Doug Bell (douglas.bell@gmail.com)
 * 
 */
public class ItemLookupResponse implements Serializable {

	/** serial version UID */
	private static final long serialVersionUID = 1L;

	/** The item description */
	private String description;
	
	/** The image src */
	private String imgSrc;

	/** The list price */
	private String listPrice;

	/** The lowest new price */
	private String lowestNewPrice;

	/** The lowest refurbished price */
	private String lowestRefurbishedPrice;

	/** The lowest used price  */
	private String lowestUsedPrice;

	/** Instnace of product {@link Bitmap} */
	private Bitmap productBitmap;

	/** The title of the item */
	private String title;

	/**
	 * Get the item description
	 * 
	 * @return {@link String}
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Get the image source url 
	 * 
	 * @return {@link String}
	 */
	public String getImgSrc() {
		return imgSrc;
	}

	/**
	 * Get the list price
	 * 
	 * @return {@link String}
	 */
	public String getListPrice() {
		return listPrice;
	}

	/**
	 * Get the lowest new price 
	 * 
	 * @return {@link String}
	 */
	public String getLowestNewPrice() {
		return lowestNewPrice;
	}

	/**
	 * Get the lowest refurbished price
	 * 
	 * @return {@link String}
	 */
	public String getLowestRefurbishedPrice() {
		return lowestRefurbishedPrice;
	}

	/**
	 * Get the lowest used price 
	 * 
	 * @return {@link String}
	 */
	public String getLowestUsedPrice() {
		return lowestUsedPrice;
	}

	/**
	 * Get the product {@link Bitmap}
	 * 
	 * @return {@link Bitmap}
	 */
	public Bitmap getProductBitmap() {
		return productBitmap;
	}

	/**
	 * Get the title
	 * 
	 * @return {@link String}
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the description 
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Set the image source
	 * 
	 * @param imgSrc
	 *            the imgSrc to set
	 */
	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}

	/**
	 * Set the list price 
	 * 
	 * @param listPrice
	 *            the listPrice to set
	 */
	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}

	/**
	 * Set the lowest new price 
	 * 
	 * @param lowestNewPrice
	 *            the lowestNewPrice to set
	 */
	public void setLowestNewPrice(String lowestNewPrice) {
		this.lowestNewPrice = lowestNewPrice;
	}

	/**
	 * Set the lowest refurbished price
	 * 
	 * @param lowestRefurbishedPrice
	 *            the lowestRefurbishedPrice to set
	 */
	public void setLowestRefurbishedPrice(String lowestRefurbishedPrice) {
		this.lowestRefurbishedPrice = lowestRefurbishedPrice;
	}

	/**
	 * Set the lowest price
	 * 
	 * @param lowestUsedPrice
	 *            the lowestUsedPrice to set
	 */
	public void setLowestUsedPrice(String lowestUsedPrice) {
		this.lowestUsedPrice = lowestUsedPrice;
	}

	/**
	 * Set the product {@link Bitmap}
	 * 
	 * @param productBitmap  The {@link Bitmap} to set
	 */
	public void setProductBitmap(Bitmap productBitmap) {
		this.productBitmap = productBitmap;
	}

	/**
	 * Set the title
	 * 
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.MULTI_LINE_STYLE);
	}

}