package com.dugbel.glass.pricethis;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;

/**
 * {@link Activity} for showing the raw item image
 * 
 * @author Doug Bell (douglas.bell@gmail.com)
 * 
 */
public class ItemLookupShowImageActivity extends Activity {

	/** The {@link Log} tag */
	private static final String TAG = "ItemLookupShowImageActivity";

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.v(TAG, "onCreate starting");
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.activity_item_lookup_show_image);

		final ImageView imageView = (ImageView) findViewById(R.id.enlargedProductImage);
		
		final byte[] bitmapData = (byte[])getIntent().getSerializableExtra("bitmapByteArray");
		final Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapData , 0, bitmapData .length);
		
		imageView.setImageBitmap(bitmap);
		
		Log.v(TAG, "onCreate complete");
		

	}

}