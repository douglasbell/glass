package com.dugbel.glass.pricethis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.mirasense.scanditsdk.ScanditSDKAutoAdjustingBarcodePicker;
import com.mirasense.scanditsdk.interfaces.ScanditSDK;
import com.mirasense.scanditsdk.interfaces.ScanditSDKListener;

/**
 * Activity that starts the barcode scanning process handing it to the 
 * {@link ItemLookupActivity} once a valid barcode has been detected 
 * 
 * @author Doug Bell (douglas.bell@gmail.com)
 * 
 */
public class PriceThisActivity extends Activity implements ScanditSDKListener {

	/** The Scandit application key */
	private static final String SCANDIT_APP_KEY = "PVTjEHAREeOKSsXAH4XW38beesERfZpXmxvtEiKl3jc";

	/** The {@link Log} tag */
	private static final String TAG = "PriceThisActivity";

	/** Instance of {@link ScanditSDK} */
	private ScanditSDK barcodePicker;

	/*
	 * (non-Javadoc)
	 * @see com.mirasense.scanditsdk.interfaces.ScanditSDKListener#didCancel()
	 */
	@Override
	public void didCancel() {
		if (barcodePicker != null) {
			barcodePicker.stopScanning();
		}
		finish();
	}

	/*
	 * (non-Javadoc)
	 * @see com.mirasense.scanditsdk.interfaces.ScanditSDKListener#didManualSearch(java.lang.String)
	 */
	@Override
	public void didManualSearch(final String barcode) {
		throw new RuntimeException("Not Implemented");
	}

	/*
	 * (non-Javadoc)
	 * @see com.mirasense.scanditsdk.interfaces.ScanditSDKListener#didScanBarcode(java.lang.String, java.lang.String)
	 */
	@Override
	public void didScanBarcode(final String barcode, final String symbology) {
		Log.d(TAG, "Scanned bardcode : " + barcode + " Symbology: " + symbology);
		// Remove non-relevant characters that might be displayed as rectangles on some devices. 
		// Only special GS1 code formats contain such characters.
		final StringBuilder cleanedBarcode = new StringBuilder();
		for (int i = 0; i < barcode.length(); i++) {
			if (barcode.charAt(i) > 30) {
				cleanedBarcode.append(barcode.charAt(i));
			}
		}

		final Intent intent = new Intent(this, ItemLookupActivity.class);
		intent.putExtra("barcode", cleanedBarcode.toString());
		intent.putExtra("symbology", symbology);

		if (barcodePicker.isScanning()) {
			barcodePicker.stopScanning();
		}
		
		startActivity(intent);
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		if (barcodePicker != null && barcodePicker.isScanning()) {
			barcodePicker.stopScanning();
		}
		finish();
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		final ScanditSDKAutoAdjustingBarcodePicker picker= new ScanditSDKAutoAdjustingBarcodePicker(
				this, SCANDIT_APP_KEY, ScanditSDKAutoAdjustingBarcodePicker.CAMERA_FACING_FRONT);

		setContentView(picker);

		barcodePicker = picker;
		barcodePicker.getOverlayView().addListener(this);
		barcodePicker.getOverlayView().showSearchBar(false);
	
		if (!barcodePicker.isScanning()) {
			barcodePicker.startScanning();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (barcodePicker != null) {
			barcodePicker.stopScanning();
		}
		finish();
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		if (barcodePicker != null && barcodePicker.isScanning()) {
			barcodePicker.stopScanning();
		}
		super.onPause();
	}


	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		if (barcodePicker != null && !barcodePicker.isScanning()) {
			barcodePicker.startScanning();
		}
		super.onResume();
	}

}