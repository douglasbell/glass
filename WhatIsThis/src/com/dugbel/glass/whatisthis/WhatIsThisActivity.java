package com.dugbel.glass.whatisthis;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Process;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.dugbel.glass.util.MultipartPost;
import com.dugbel.glass.util.PostParameter;
import com.google.android.glass.app.Card;
import com.google.android.glass.timeline.TimelineManager;

/**
 * Main activity.
 */
public class WhatIsThisActivity extends Activity {

	private static TimelineManager tlm;

	private static Card _card;
	private static View _cardView;
	private static TextToSpeech _speech;

	private Context _context = this;

	private static final String TAG = WhatIsThisActivity.class.getSimpleName();

	private static final String IMAGE_FILE_NAME = Environment.getExternalStorageDirectory().getPath()  + "/Pictures/product_image.jpg";

	private boolean picTaken = false; // flag to indicate if we just returned from the picture taking intent
	protected boolean mbActive;

	final Handler myHandler = new Handler(); // handles looking for the returned image file

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		tlm = TimelineManager.from(this);

		_card = new Card(_context);
		_card.setFootnote("Processing Image");

		_cardView = _card.toView();
		_cardView.setKeepScreenOn(true);
		setContentView(_cardView);

	}

	/**
	 * Handle the tap event from the touchpad.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_ENTER) {

			// Change the text of the card when the touchpad is touched
			_cardView = _card.toView();
			setContentView(_cardView);

			return true;

		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!picTaken) {
			Intent intent = new Intent(this, SnapshotActivity.class);
			intent.putExtra("imageFileName",IMAGE_FILE_NAME);
			intent.putExtra("previewWidth", 640);
			intent.putExtra("previewHeight", 360);
			intent.putExtra("snapshotWidth", 1280);
			intent.putExtra("snapshotHeight", 720);
			intent.putExtra("maxWaitForCameraInMs", 2000);
			startActivityForResult(intent,1);
		}
		else {
			// do nothing
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//Close the Text to Speech Library
		if(_speech != null) {
			_speech.stop();
			_speech.shutdown();
			_speech = null;
			Log.d(TAG, "TTS Destroyed");
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		picTaken = true;
		switch(requestCode) {
		case (1) : {
			if (resultCode == Activity.RESULT_OK) {
				// TODO Extract the data returned from the child Activity.
				Log.v(TAG,"onActivityResult"); 

				File f = new File(IMAGE_FILE_NAME);
				if (f.exists()) {
					Log.v(TAG,"image file from camera was found");

					//setContentView(R.layout.activity_item_lookup_interstitial);

					final ImageSearchTask imageSearchTask = new ImageSearchTask();
					imageSearchTask.execute("");

					setContentView(R.layout.activity_item_lookup_interstitial);

				}
			}
			else {
				Log.v(TAG,"onActivityResult returned bad result code");
				finish();
			}
			break;
		} 
		}
	}




	/**
	 * {@link AsyncTask} that makes the call to the Google image search
	 * URL, parsing and returning the information to the screen.
	 * 
	 * @author Doug Bell (douglas.bell@gmail.com)
	 *
	 */
	class ImageSearchTask extends AsyncTask<String, Integer, String> {


		/** The {@link Log} tag */
		private static final String TAG = "ImageSearchTask";

		/* (non-Javadoc)
		 * @see android.os.AsyncTask#doInBackground(Params[])
		 */
		@Override
		protected String doInBackground(String... params) {

			Log.v(TAG, "doInBackground start");
			// Give the task a bit more horsepower 
			Process.setThreadPriority(Process.THREAD_PRIORITY_FOREGROUND);

			String tempresult = null;
			File file = new File(IMAGE_FILE_NAME);
			String postResult = postData(file);

			boolean best_guess_found=false;
			StringTokenizer tok = new StringTokenizer(postResult, "<>");
			String previous_entity=null;

			while(tok.hasMoreElements()){
				String nextitem = tok.nextElement().toString();

				if (best_guess_found==false && nextitem.startsWith("Best guess for this image")){
					Log.d("Tokenizer", nextitem);
					best_guess_found=true;
				} else if (best_guess_found==true && nextitem.contains("q=") && nextitem.contains("&amp")){
					int start = nextitem.indexOf("q=")+2;
					int end = nextitem.indexOf("&amp", start);
					String contents = previous_entity.substring( start , end);
					contents = contents.replace('+', ' ');
					Log.d("Result:", contents);

					tempresult = contents;
					break;
				} else if(nextitem.startsWith("Visually similar") && best_guess_found==false){
					Log.d("Tokenizer", "nextitem: " + nextitem + " previousitem: " + previous_entity);
					try{
						if(previous_entity.contains("q=") && previous_entity.contains("&amp")){
							int start = previous_entity.indexOf("q=")+2;
							int end = previous_entity.indexOf("&amp", start);
							String contents = previous_entity.substring( start , end);
							contents = contents.replace('+', ' ');

							Log.d("Result:", contents);

							tempresult = contents;
						} else {


						}
					} catch (Exception e){
						e.printStackTrace();						
					}
					break;
				}

				if(nextitem.startsWith("a")){
					StringTokenizer tok2 = new StringTokenizer(nextitem);

					while(tok2.hasMoreElements()){
						String subitem = tok2.nextElement().toString();
						if( subitem.startsWith("href") ){
							previous_entity=nextitem;
						}
					}
				}
			}

			Log.d(TAG, "doInBackground complete");

			return tempresult;
		}

		public String postData(final File image) {
			Log.v(TAG, "postData start");

			final StringBuilder src = new StringBuilder();

			// Create a new HttpClient and Post Header
			final HttpClientBuilder builder = HttpClientBuilder.create();
			final CloseableHttpClient httpClient = builder.build();
			
			HttpPost httpPost = new HttpPost("http://www.google.com/searchbyimage/upload");

			httpPost.addHeader("Referer", "http://images.google.com");


			Log.v(TAG, "Image Size " + image.length() + " bytes");

			try {
				
				MultipartEntityBuilder entity = MultipartEntityBuilder.create();
				
				entity.addTextBody("image_url", "");
				entity.addTextBody("btnG", "Search");
				entity.addTextBody("image_content", "");
				entity.addTextBody("filename", "");
				entity.addTextBody("hl", "en");
				entity.addTextBody("safe", "off");
				entity.addTextBody("bih", "");
				entity.addTextBody("biw", "");
				entity.addBinaryBody("encoded_image", image);

				httpPost.setEntity(entity.build());

				HttpResponse response = httpClient.execute(httpPost);

				int returnCode = response.getStatusLine().getStatusCode();

				if(returnCode == HttpStatus.SC_OK) {


					BufferedReader reader = null;
					try {
						reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

						final char[] buffer = new char[4 * 1024];
						int charsRead;
						while ((charsRead = reader.read(buffer)) != -1) {
							src.append(buffer, 0, charsRead);
						}    

					} catch (IOException e) {
						Log.e(TAG, e.getMessage());
					} finally {

						if(reader != null) {
							try {
								reader.close(); 
							} catch (IOException e2) {
								Log.e(TAG,"Could not close Reader. " + e2.getMessage(), e2);
							}
						}
					}
				} else {
					throw new HttpException("Did not receive a valid response. HTTP status: " + returnCode);
				}

			} catch (ClientProtocolException e) {
				Log.e(TAG, e.getMessage());
			} catch (HttpException e) {
				Log.e(TAG, e.getMessage());
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
			} finally {
				if (httpClient != null) {
					try {
						httpClient.close();
					} catch (IOException e) {
						Log.e(TAG, "Could not close HttpClient. " + e.getMessage());
					}
				}
			}

			Log.v(TAG, "RESPONSE: " + src.toString());

			Log.v(TAG, "postData complete");

			return src.toString();
		} 

		private  String _sendPostRequest(File image) {
			Log.v(TAG, "POST Request Start");

			String response = null;

			Log.d("SendPostRequest", "sendPostRequest");
			@SuppressWarnings("rawtypes")
			List<PostParameter> params = new ArrayList<PostParameter>();
			params.add(new PostParameter<String>("image_url", ""));
			params.add(new PostParameter<String>("btnG", "Search"));
			params.add(new PostParameter<String>("image_content", ""));
			params.add(new PostParameter<String>("filename", ""));
			params.add(new PostParameter<String>("hl", "en"));
			params.add(new PostParameter<String>("safe", "off"));
			params.add(new PostParameter<String>("bih", ""));
			params.add(new PostParameter<String>("biw", ""));
			params.add(new PostParameter<File>("encoded_image", image));

			try {
				Log.d(TAG, "multipart post create start");
				MultipartPost post = new MultipartPost(params);
				Log.d(TAG, "multipart post create complete");
				response = post.send("http://www.google.com/searchbyimage/upload", "http://images.google.com");
				Log.v(TAG, "RAW RESPONSE: " + response);
			} catch (Exception e) {
				Log.e(TAG, "Bad Post", e);
			}

			params.clear();
			params = null;

			Log.v(TAG, "POST Request Complete");

			return response;
		} 


		/* (non-Javadoc)
		 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
		 */
		@Override
		protected void onPostExecute(String results) {
			Log.d(TAG, "onPostExecute start");

			Card card = new Card(_context);
			card.setImageLayout(Card.ImageLayout.FULL);
			File file = new File(IMAGE_FILE_NAME);
			card.addImage(Uri.fromFile(file));


			if(results!=null){
				card.setFootnote("Image Info: " + results);
				//_speech.speak("You are viewing " + results, TextToSpeech.QUEUE_FLUSH, null);
			} else {
				card.setFootnote("No results found.");
				//_speech.speak("No results found." , TextToSpeech.QUEUE_FLUSH, null);
			}

			setContentView(card.toView());
			//tlm.insert(card); //TODO add to timeline?

			Log.d(TAG, "onPostExecute Complete");
		}

	};

}